# README

To run the project:

1. Get AWS credentials (see https://aws.amazon.com/premiumsupport/knowledge-center/create-access-key/)

2. Set the following environment variables:

   AWS_ACCESS_KEY: the access key for the IAM User
   AWS_SECRET_KEY: the secret key for the IAM User
   AWS_DEFAULT_REGION: a valid region code for AWS, e.g. us-east-1

   ```
   AWS_ACCESS_KEY=AAAAAAAAAAA
   AWS_SECRET_KEY=1234567890ABCDEF123456
   AWS_DEFAULT_REGION=us-east-1
   export AWS_ACCESS_KEY AWS_SECRET_KEY AWS_DEFAULT_REGION
   ```

3. To setup ansible, run:

   ```
   virtualenv .venv -p /usr/bin/python3
   source .venv/bin/activate
   pip install -r requirements.txt
   ```

4. Execute ansible:

   ```
   ansible-playbook -i localhost sites.yml -v
   ```

   